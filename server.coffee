http = require 'http'
fs = require 'fs'
url = require 'url'

content_types =
  'html': 'text/html'
  'css': 'text/css'
  'js': 'text/javascript'

http.createServer (request, response) ->
  console.log request.url
  filePath = url.parse(request.url).pathname.substring 1

  fns =
    '/': () -> respond 'index.html', 200
    '/upload': () -> respond 'uploaded.html', 200

  respond = (file, status) ->
    extension = file.replace /^.*\.(.*)$/, '$1'
    console.log "extension: #{extension}"
    fs.readFile file, (err, contents) ->
      if err
        console.log err
        respond '500.html', 500
        return
      else if content_types[extension]
        response.writeHead status,
          'Content-Length': contents.length
          'Content-Type': content_types[extension]
        response.end contents
      else
        respond '404.html', 404

  fs.lstat filePath, (err, stats) ->
    if fns[request.url]
      fns[request.url]()
    else if err or stats.isDirectory()
      respond '404.html', 404
    else
      respond filePath, 200

.listen 8080
